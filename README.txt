
-- SUMMARY --

Brow.si is a groundbreaking user engagement Drupal module for the mobile web.
It provides web publishers with powerful extensions that significantly enhance
the user experience and engagement with respect to their sites and content on
mobile devices. Moreover, the Brow.si module assists publishers by having a
simple installation to retain mobile users and unlock new revenue opportunities.
From advanced shared tools, to content push notifications, Brow.si is the
perfect enhancement to your current Drupal modules list.
Brow.si is a one click customisable module. It is extremely lightweight and can
easily be integrated to any site at any vertical or domain. Most importantly,
Brow.si fits seamlessly in the mobile browsing experience, adding real value to
both the publisher and the user without requiring the publisher to make any
changes to the site.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --

* Configure your site key in Administration » Configuration » Brow.si settings:
  You can get your Site Key by registering at https://brow.si

-- CONTACT --

Brow.si support is available by mail:
support@brow.si
