<?php
/**
 * @file
 * Browsi module admin functions.
 */

define('BROWSI_SITE_KEY_LENGTH', 6);

/**
 * Form constructor for the Brow.si settings page.
 *
 * @see system_settings_form()
 */
function browsi_admin_settings() {
  $form = array(
    'browsi_key' => array(
      '#type' => 'textfield',
      '#title' => t('Brow.si site key'),
      '#default_value' => variable_get('browsi_key', ''),
      '#size' => BROWSI_SITE_KEY_LENGTH,
      '#maxlength' => BROWSI_SITE_KEY_LENGTH,
      '#element_validate' => array('_browsi_admin_settings_browsi_key'),
      '#description' =>
      t("An unique identifier for this site as defined in <a href=\"@browsi_dashboard\" target=\"_blank\">Brow.si dashboard</a>. If left empty, analytics data won't be collected for this site.", array('@browsi_dashboard' => 'https://brow.si/dashboard')),
      '#required' => FALSE,
    ),
  );
  return system_settings_form($form);
}

/**
 * Validation function for the admin.
 */
function _browsi_admin_settings_browsi_key($element, &$form_state) {
  $key_length = BROWSI_SITE_KEY_LENGTH;
  if (!empty($element['#value']) && strlen($element['#value']) != $key_length) {
    form_error($element, t('Site key must be in length of %length characters', array('%length' => $key_length)));
  }
}
